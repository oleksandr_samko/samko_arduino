
void setup()
{
  pinMode(A0,INPUT);
  Serial.begin(9600); 
}

void loop()
{
 // Зчитування даних з датчика.
 //напруга зберігається як 10-бітове число.
  int acp = analogRead(A0);
//перетворення 10-бітового число в вольти напруги за формулою
  float voltage = (acp * 5.0)/1024;
 //мілівольти
  float milliVolt = voltage * 1000;
  //опір
  float resistance = 10 * voltage / 5 ;
  Serial.print("ACP 10bit:  ");
  Serial.println(acp);
  
  Serial.print("voltage V: ");
  Serial.print(voltage);
  Serial.println("V");
  
  Serial.print("mili voltage: ");
  Serial.print(milliVolt);
  Serial.println("mV");
  
  Serial.print("Resistance: ");
  Serial.print(resistance);
  Serial.println("Om");
  
  Serial.println();
  delay(1000);
}