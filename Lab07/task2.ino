
void setup()
{
  pinMode(A0,INPUT);
  Serial.begin(9600); 
}
/*
	TMP36  почті тоже что LM35 - датчик температури
*/

void loop()
{
 // Зчитування даних з датчика.
 //напруга зберігається як 10-бітове число.
  int acp = analogRead(A0);
//перетворення 10-бітового число в вольти напруги за формулою
  float voltage = (acp * 5.0)/1024;
 //мілівольти
  float milliVolt = voltage * 1000;
  //температура(−40°C - +125°C) з 10біт в цельсії
  float tmpCel =  (milliVolt-500)/10 ;
  
  Serial.print("ACP 10bit:  ");
  Serial.println(acp);
  
  Serial.print("voltage V: ");
  Serial.print(voltage);
  Serial.println("V");
  
  Serial.print("mili voltage: ");
  Serial.print(milliVolt);
  Serial.println("mV");
  
  Serial.print("Temperature: ");
  Serial.print(tmpCel);
  Serial.println(" c0");
  Serial.println();
  delay(1000);
}