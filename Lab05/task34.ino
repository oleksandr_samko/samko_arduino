int ledPin = 13;

void setup() {
  pinMode(ledPin, OUTPUT);
}
void loop() {
  for(int x=0; x<3; x++){
    digitalWrite(ledPin, LOW);
    delay(250);
    digitalWrite(ledPin, HIGH);
    delay(100);
  }
  delay(100);
  for(int x=0; x<3; x++){
     digitalWrite(ledPin, LOW);
     delay(750);
     digitalWrite(ledPin, HIGH);
     delay(100);
  }
  delay(100);
  for(int x=0; x<3; x++){
     digitalWrite(ledPin, LOW);
     delay(250);
     digitalWrite(ledPin, HIGH);
     delay(100);
  }
  delay(3000);
}
