int led = 13;
int volume = 3;
int sensor  = 9;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(volume, OUTPUT);
  pinMode(sensor, INPUT);
}

void loop() {
  int val = digitalRead(sensor);
  if(val == 1) {
    digitalWrite(led, HIGH);
    digitalWrite(volume, HIGH);
  } else {
    digitalWrite(led, LOW); 
    digitalWrite(volume, LOW);  
    Serial.println("Vibration");
    delay(500); 
  }
}

